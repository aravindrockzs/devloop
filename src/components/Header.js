import React from 'react'

import classNames from 'classnames/bind';
import styles from './Header.module.css'

const cx = classNames.bind(styles);

const Header = () => {

	return (
		<div className={cx('header-container')}>
			<div className={cx('logo-navbar')}> 
				<div className={cx('logo-container')}>

					Devloop


				</div>

				<div className={cx('nav-container')}>

						<nav className={cx('nav-left')}>
							<div className={cx('avatar-container')}>
								<img className={cx('avatar-image' )} alt="avatar" 
								 src="https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png"/>
							</div>
							<li>Site</li>
							<li>View</li>
							<li>Tools</li>
							<li>Dev Mode</li>
							<li>Help</li>
							<li>Upgrade</li>
						</nav>

						<nav className={cx('nav-right')}>

							<li>Invite</li>
							<li className={cx('preview-btn')}>Preview</li>
							<button className={cx('publish-cta')}> Publish</button>



						</nav>




				</div>

			</div>



		</div>
	)
}




export default Header;